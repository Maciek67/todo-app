import React, { Component } from 'react';
import AddTask from './AddTask';
import TaskList from './TaskList';


import './App.css';

class App extends Component {
  counter = 6;
  state = {
    tasks: [
      {
        id: 0,
        text: 'zagrać w Wiedźmina',
        date: '2021-10-30',
        important: true,
        active: true,
        finishDate: null
      },
      {id: 1, text: 'zrobić dobry uczynek', date: '2021-11-12', important: true, active: true, finishDate: null},
      {id: 2, text: 'wypić kawę', date: '2021-11-13', important: false, active: true, finishDate: null}, 
      {id: 3, text: 'schudnąć 30 kg', date: '2021-12-12', important: false, active: true, finishDate: null},
      {id: 4, text: 'wyjść z psem', date: '2021-12-15', important: false, active: true, finishDate: null},
      {id: 5, text: 'naprawić lodówkę', date: '2021-12-22', important: true, active: true, finishDate: null}
    ]
  }

  deleteTask = (id) => {
    console.log('delete elementu o id' + id);

    // const tasks = [...this.state.tasks];
    // console.log(tasks)
    // const index = tasks.findIndex(task => task.id === id);
    // tasks.splice(index, 1);

    // this.setState({
    //   tasks
    // });
    let tasks = [...this.state.tasks];
    tasks = tasks.filter(task => task.id !== id);
    console.log (tasks);
    this.setState({
      tasks
    });


    
     
  }

  changeTaskStatus = (id) => {
    console.log('change w stanie elementu o id' + id);
    const tasks = Array.from(this.state.tasks);
    tasks.forEach(task => {
      if(task.id === id) {
        task.active = false;
        task.finishDate = new Date().getTime();
      }
    });
    this.setState({
      tasks
    })

  }
  addTask = (text, date, important) => {
    
    const task = {
        id: this.counter,
        text,//tekst z inputa
        date,//tekst z inputa
        important,//wartość z inputa
        active: true,
        finishDate: null
    }

    this.counter++
    console.log(task, this.counter);
    this.setState(prevState => ({
      tasks: [...prevState.tasks, task]
    }))

    return true
  }

  render() {
    return (
      <div className="App">
      <h1>TODO APP</h1>
      <AddTask add={this.addTask} />
      <TaskList tasks={this.state.tasks} delete={this.deleteTask} change={this.changeTaskStatus} />
      </div>
    );
  }
}

export default App;
